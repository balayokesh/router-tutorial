import { Link } from 'react-router-dom';

function App() {
  return (
    <>
    <h1>This is the app page</h1>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
        <Link to="/project">Project</Link>
        </li>
        <li>
        <Link to="/contact">Contact</Link>
        </li>
      </ul>
    </ >
  );
}

export default App;
