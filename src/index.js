import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Route, BrowserRouter, Link, Switch } from 'react-router-dom';
import Project from './Components/Projects';
import Contact from './Components/Contact';

const Routing = () => {
  return (
    <BrowserRouter>
      <Route path="/" exact component={App} />
      <Route path="/project" component={Project} />
      <Route path="/contact" component={Contact} />
    </BrowserRouter>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <Routing />
  </React.StrictMode>,
  document.getElementById('root')
);
